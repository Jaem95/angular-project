﻿

var myApp = angular.module('purchaseApp', ['ngSanitize']);

myApp.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);
myApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
}]);


angular.module('purchaseApp', ['ngSanitize']).controller("purchaseController", function ($scope, $http) {

    $http.get('http://localhost:9080/Test/bringPurchase/').then((response) => {
        document.getElementById("bind").innerHTML = response.data;
    });

    $scope.Confirmar = function () {
        var saldo = document.getElementById("saldo").innerHTML;
        var total = document.getElementById("total").innerHTML;


        if (parseInt(total) > parseInt(saldo)) {

            swal({
                title: "Compra Incorrecta",
                text: "No tiene saldo suficiente para  comprar estos productos ¿Desea volver?",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si!',
                cancelButtonText: 'No!'
            }).then(function () {
                window.location = 'Shop.html';
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
                if (dismiss === 'cancel') {
                    swal(
                      'Ni modo :(',
                      '',
                      'error'
                    )
                }
            });
        }
        else {
            swal({
                title: "¿En verdad desea realizar la compra?",
                text: "No hay vuelta atras",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si!',
                cancelButtonText: 'No!'
            }).then(function () {
                //confirmPurchase
                $http.get('http://localhost:9080/Test/confirmPurchase/'+saldo+'/'+total).then((response) => {
                    if (response.data == 1) {
                        swal({
                            title: "¿Compra realizada con exito!",
                            text: "Gracias por la compra!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok!',
                            cancelButtonText: 'No!'
                        }).then(function () {
                            window.location = '/Shop.html';

                        });
                    }
                });
               
            });
        }
    }


});