﻿

var myApp = angular.module('profileApp', []);


myApp.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);
myApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
}]);


angular.module('profileApp').controller("profileController", function ($scope, $http) {

    $http.get('http://localhost:9080/Test/bringInfo/').then((response) => {
        console.log(response.data);
        var str = response.data;
        var res = str.split("=");

        

        var str2 = res[0];
        var res2 = str2.replace("-", " ");

        $scope.nombre = res2;
        $scope.contra = res[1];
        $scope.email = res[2];
    });

    $scope.submit = function () {
        $http.get('http://localhost:9080/Test/editUser/' + $scope.nombre + '/' + $scope.email + '/' + $scope.contra + '').then(function (response) {
            var data = response.data;
            var status = response.status;
            var statusText = response.statusText;
            var headers = response.headers;
            var config = response.config;
            if (data == "1") {
                swal({
                    title: 'Modificacion Exitosa!',
                    text: "Bienvenido!",
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'OK'
                }).then(function () {
                    window.location = "Shop.html"
                });
            }

        });

    }


});