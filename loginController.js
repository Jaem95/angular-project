﻿

var myApp = angular.module('finalApp', []);

myApp.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

angular.module('finalApp').controller("loginController", function ($scope, $http) {


    $scope.customerData = [];

    $scope.login = function (e,p) {
        $http.get('http://localhost:9080/Test/userAngular/' + e + '/' + p + '')
          .then(function (response) {

              var data = response.data;
              var status = response.status;
              var statusText = response.statusText;
              var headers = response.headers;
              var config = response.config;
              console.log(data);
              if (data > 0) {
                  swal({
                      title: 'Inicio de Sesion Exitoso',
                      text: 'Bienvenido!',
                      type: 'success'
                  }).then(function () {
                      window.location = "Shop.html"
                  });
              }
              else {
                  swal({
                      title: 'Oops...Usuario Inexistente',
                      text: "¿Deseas registrarte?",
                      type: 'error',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Si!',
                      cancelButtonText: 'No!'
                  }).then(function () {
                      window.location = "Register.html"
                      
                  })
              }
              
          });

    };


});